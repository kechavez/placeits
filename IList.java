import java.util.*;

public abstract class IList
{
	private HashMap<String, HashMap> PlaceItStatus = new HashMap<String, HashMap>();
	private HashMap<String, PlaceIt> sundayPlaceIts = new HashMap<String, PlaceIt>();
	private HashMap<String, PlaceIt> mondayPlaceIts = new HashMap<String, PlaceIt>();
	private HashMap<String, PlaceIt> tuesdayPlaceIts = new HashMap<String, PlaceIt>();
	private HashMap<String, PlaceIt> wednesdayPlaceIts = new HashMap<String, PlaceIt>();
	private HashMap<String, PlaceIt> thursdayPlaceIts = new HashMap<String, PlaceIt>();
	private HashMap<String, PlaceIt> fridayPlaceIts = new HashMap<String, PlaceIt>();
	private HashMap<String, PlaceIt> saturdayPlaceIts = new HashMap<String, PlaceIt>();
	
	public IList()
	{
		PlaceItStatus.put("Sunday", sundayPlaceIts);
		PlaceItStatus.put("Monday", mondayPlaceIts);
		PlaceItStatus.put("Tuesday", tuesdayPlaceIts);
		PlaceItStatus.put("Wednesday", wednesdayPlaceIts);
		PlaceItStatus.put("Thursday", thursdayPlaceIts);
		PlaceItStatus.put("Friday", fridayPlaceIts);
		PlaceItStatus.put("Saturday", saturdayPlaceIts);
	}
	// public abstract void create(String day, PlaceIt p);
	public void create(String day, PlaceIt p)
	{
		PlaceItStatus.get(day).put(p.toString(), p);
	}
	
	//public abstract void delete(String key, String day);
	public void delete(String key, String day)
	{
		PlaceItStatus.get(day).remove(key);
	}
	public abstract void print();
}
